from django.shortcuts import render
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Reg
from .serializers import RegSerializers

#list
# Create your views here.
class RegList(APIView):
    def get(self,request):
        regs=Reg.objects.all()
        serializer=RegSerializers(regs,many=True)
        return Response(serializer.data)
   # def gets(self,request):
   #     regs1=Reg.objects.filter(id=1)
   #     ser=RegSerializers(regs1,many=True)
   #     return Response(ser.data)
    def post(self):
        pass
class RegUser(APIView):
    def get(self,request,id_user):
        id_users=int(id_user)
        regs=Reg.objects.filter(id=id_users)
        serializer=RegSerializers(regs,many=True)
        return  Response(serializer.data)
